package com.rahasak.etcd4s.repo

import cats.effect.IO
import com.rahasak.etcd4s.protocol.Service

trait EtcdRepo {

  def addService(service: Service): IO[Option[String]]

  def getService(name: String): IO[Option[Service]]

  def updateService(service: Service): IO[Option[String]]

  def getServices(): IO[Seq[Service]]

  def deleteService(service: String): IO[Long]

}
