package com.rahasak.etcd4s.repo

import cats.effect.{ContextShift, IO}
import com.rahasak.etcd4s.protocol.Service
import org.etcd4s.formats._
import org.etcd4s.{Etcd4sClient, Etcd4sClientConfig}

import scala.concurrent.ExecutionContext

class EtcdRepoImpl(config: Etcd4sClientConfig)(implicit ec: ExecutionContext, cs: ContextShift[IO])
  extends EtcdRepo {

  override def addService(service: Service): IO[Option[String]] = {
    val client = Etcd4sClient.newClient(config)
    IO.fromFuture(
      IO(client.kvService.setKey(s"services/${service.name}", service.status)
        .map(kv => kv.map(_.key.toStringUtf8)))
    )
  }

  override def getService(name: String): IO[Option[Service]] = {
    val client = Etcd4sClient.newClient(config)
    IO.fromFuture(
      IO(client.kvService.getKey(s"services/$name")
        .map(t => t.map(v => Service(name, v))))
    )
  }

  override def updateService(service: Service): IO[Option[String]] = {
    val client = Etcd4sClient.newClient(config)
    IO.fromFuture(
      IO(client.kvService.setKey(s"services/${service.name}", service.status)
        .map(kv => kv.map(_.key.toStringUtf8)))
    )
  }

  override def getServices(): IO[Seq[Service]] = {
    val client = Etcd4sClient.newClient(config)
    IO.fromFuture(
      IO(client.kvService.getRange(s"services/")
        .map(_.kvs.map(a => Service(a.key.toStringUtf8.split("/").last, a.value.toStringUtf8))))
    )
  }

  override def deleteService(service: String): IO[Long] = {
    val client = Etcd4sClient.newClient(config)
    IO.fromFuture(
      IO(client.kvService.deleteKey(s"services/$service")
        .map(p => p))
    )
  }

}
