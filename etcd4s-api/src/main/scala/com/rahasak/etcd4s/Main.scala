package com.rahasak.etcd4s

import cats.effect.IO
import com.rahasak.etcd4s.config.Config
import com.rahasak.etcd4s.protocol.Service
import com.rahasak.etcd4s.repo.EtcdRepoImpl
import org.etcd4s.Etcd4sClientConfig


object Main extends App {

  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
  implicit val cs = IO.contextShift(ec)

  // create etcd repo
  val etcdRepo = for {
    config <- Config.load()
    repo <- IO(new EtcdRepoImpl(Etcd4sClientConfig(config.host, config.port)))
  } yield repo

  // add service
  val status1 = for {
    repo <- etcdRepo
    s <- repo.addService(Service("octopus", "pending"))
  } yield s
  println(status1.unsafeRunSync())

  // output
  // None


  // add service
  val status2 = for {
    repo <- etcdRepo
    s <- repo.addService(Service("siddhi", "pending"))
  } yield s
  println(status2.unsafeRunSync())

  // output
  // None


  // update service
  val status3 = for {
    repo <- etcdRepo
    s <- repo.updateService(Service("octopus", "running"))
  } yield s
  println(status3.unsafeRunSync())

  // output
  // None


  // get service
  val service = for {
    repo <- etcdRepo
    s <- repo.getService("octopus")
  } yield s
  println(service.unsafeRunSync())

  // output
  // Some(Service(octopus,running))


  // get all services
  val services = for {
    repo <- etcdRepo
    result <- repo.getServices()
  } yield result
  println(services.unsafeRunSync())

  // output
  // Vector(Service(octopus,running), Service(siddhi,pending))


  // delete service
  val status4 = for {
    repo <- etcdRepo
    result <- repo.deleteService("octopus")
  } yield result
  println(status4.unsafeRunSync())

  // output
  // 1

}
