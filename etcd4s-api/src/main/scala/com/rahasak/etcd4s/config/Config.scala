package com.rahasak.etcd4s.config

import cats.effect.IO
import io.circe.config.parser
import io.circe.generic.auto._

case class EtcdConfig(host: String, port: Int)

object Config {
  def load(): IO[EtcdConfig] = {
    for {
      etcdConf <- parser.decodePathF[IO, EtcdConfig]("etcd")
    } yield etcdConf
  }
}
